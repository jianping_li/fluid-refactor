### Candidate Chinese Name:
* 李建平
 
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 1、定义两个interface （OCP）其中JmsMessageBrokerService用于应用与中间件的交互、JmsMqExecutor用于执行发、收动作（SRP）。
2、将与业务逻辑不相关的类单独定义（SRP）
3、如果要集成IBM MQ直接实现JmsMessageBrokerService、JmsMqExecutor这两个接口就好
- - -