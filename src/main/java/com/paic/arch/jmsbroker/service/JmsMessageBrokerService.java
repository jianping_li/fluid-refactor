package com.paic.arch.jmsbroker.service;

/**
 * Created by lijianping on 2018/4/11.
 */
public interface JmsMessageBrokerService {

    /**
     * 启动中间件
     * @throws Exception
     */
    void startEmbeddedBroker() throws Exception;

    /**
     * 停止中间件
     * @throws Exception
     */
    void stopRunningBroker() throws Exception;

    /**
     * 发送消息到消息队列
     * @param aDestinationName
     * @param inputMessage
     * @throws Exception
     */
    void sendMessage(String aDestinationName, final String inputMessage) throws Exception;

    /**
     * 查询消息
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    String waitForAMessageOn(String aDestinationName) throws Exception;
}
