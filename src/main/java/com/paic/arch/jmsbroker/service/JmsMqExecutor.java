package com.paic.arch.jmsbroker.service;

/**
 * Created by lijianping on 2018/4/11.
 */
public interface JmsMqExecutor {

    /**
     * 发送消息到消息队列
     * @param aBrokerUrl
     * @param aDestinationName
     * @param inputMessage
     * @return
     * @throws Exception
     */
    String sendASingleMessageToTheDestination(String aBrokerUrl, String aDestinationName, String inputMessage) throws Exception;

    /**
     * 获取消息从消息队列
     * @param aBrokerUrl
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    String retrieveASingleMessageFromTheDestination(String aBrokerUrl, String aDestinationName) throws Exception;
}
