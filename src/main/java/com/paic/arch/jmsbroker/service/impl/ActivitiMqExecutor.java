package com.paic.arch.jmsbroker.service.impl;

import com.paic.arch.exception.NoMessageReceivedException;
import com.paic.arch.jmsbroker.service.JmsCallback;
import com.paic.arch.jmsbroker.service.JmsMqExecutor;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;

import javax.jms.*;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * ActivitiMQ收发器
 * Created by lijianping on 2018/4/12.
 */
public class ActivitiMqExecutor implements JmsMqExecutor {
    private static final Logger LOG = getLogger(ActivitiMqExecutor.class);
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * 1000;

    @Override
    public String sendASingleMessageToTheDestination(String aBrokerUrl, String aDestinationName, String inputMessage) throws Exception {
        return executeCallbackAgainstRemoteBroker(aBrokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(inputMessage));
            producer.close();
            return "";
        });
    }


    @Override
    public String retrieveASingleMessageFromTheDestination(String aBrokerUrl, String aDestinationName) throws Exception {
        return executeCallbackAgainstRemoteBroker(aBrokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(DEFAULT_RECEIVE_TIMEOUT);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", DEFAULT_RECEIVE_TIMEOUT));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }


    private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) throws Exception {
        Connection connection = null;
        String returnValue = "";
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            connection = connectionFactory.createConnection();
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
            return returnValue;
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new RuntimeException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new RuntimeException(jmse);
                }
            }
        }
    }

    private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) throws Exception {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            return aCallback.performJmsFunction(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new RuntimeException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new RuntimeException(jmse);
                }
            }
        }
    }
}
