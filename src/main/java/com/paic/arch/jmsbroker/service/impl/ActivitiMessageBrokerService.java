package com.paic.arch.jmsbroker.service.impl;

import com.paic.arch.jmsbroker.service.JmsMessageBrokerService;
import com.paic.arch.jmsbroker.service.JmsMqExecutor;
import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import javax.jms.MessageProducer;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by lijianping on 2018/4/12.
 */
public class ActivitiMessageBrokerService implements JmsMessageBrokerService {

    private static final Logger LOG = getLogger(ActivitiMessageBrokerService.class);
    public static final String DEFAULT_BROKER_URL = "tcp://localhost:61615";

    BrokerService brokerService = new BrokerService();//重构这里要用依赖注入

    JmsMqExecutor activitiJmsMqExecutor = new ActivitiMqExecutor();//重构这里要用依赖注入

    @Override
    public void startEmbeddedBroker() throws Exception {
        LOG.debug("启动Activiti MQ中间件...");
        brokerService.setPersistent(false);
        brokerService.addConnector(DEFAULT_BROKER_URL);
        brokerService.start();
    }

    @Override
    public void stopRunningBroker() throws Exception {
        LOG.debug("关闭Activiti MQ中间件...");
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    @Override
    public void sendMessage(String aDestinationName, final String inputMessage) throws Exception {
        activitiJmsMqExecutor.sendASingleMessageToTheDestination(DEFAULT_BROKER_URL, aDestinationName, inputMessage);

    }

    @Override
    public String waitForAMessageOn(String aDestinationName) throws Exception {
        return activitiJmsMqExecutor.retrieveASingleMessageFromTheDestination(DEFAULT_BROKER_URL, aDestinationName);
    }

}
