package com.paic.arch.jmsbroker.service;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/**
 * Created by lijianping on 2018/4/11.
 */
public interface JmsCallback {
    String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
}
