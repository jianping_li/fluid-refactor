package com.paic.arch.exception;

/**
 * Created by lijianping on 2018-4-11.
 */
public class NoMessageReceivedException extends RuntimeException {
    public NoMessageReceivedException(String message) {
        super(message);
    }
}


