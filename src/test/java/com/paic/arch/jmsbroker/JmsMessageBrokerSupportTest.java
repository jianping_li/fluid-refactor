package com.paic.arch.jmsbroker;

import com.paic.arch.exception.NoMessageReceivedException;
import com.paic.arch.jmsbroker.service.JmsMessageBrokerService;
import com.paic.arch.jmsbroker.service.impl.ActivitiMessageBrokerService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";//重构这里要用依赖注入
    public static final String MESSAGE_CONTENT = "Lorem blah blah";//重构这里要用依赖注入

    static JmsMessageBrokerService activitiMessageBrokerService = new ActivitiMessageBrokerService();


    @BeforeClass
    public static void setup() throws Exception {
        activitiMessageBrokerService.startEmbeddedBroker();
    }

    @AfterClass
    public static void teardown() throws Exception {
        activitiMessageBrokerService.stopRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        activitiMessageBrokerService.sendMessage(TEST_QUEUE, MESSAGE_CONTENT);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        activitiMessageBrokerService.sendMessage(TEST_QUEUE, MESSAGE_CONTENT);
        String receivedMessage = activitiMessageBrokerService.waitForAMessageOn(TEST_QUEUE);
        Assert.assertEquals(receivedMessage, MESSAGE_CONTENT);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        activitiMessageBrokerService.waitForAMessageOn(TEST_QUEUE);
    }


}